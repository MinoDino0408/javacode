import java.util.HashSet;
import java.util.Set;
import java.io.*;
import java.util.*;

public class SumEqualToX_02
{

    static boolean findSumOfTwo(int[] A, int target)
    {
        Set<Integer> foundValues = new HashSet<Integer>();
        for (int a : A) {
            if (foundValues.contains(target - a))
            {
                return true;
            }
            foundValues.add(a);
        }
        return false;
    }

    static boolean find_sum_of_two(int[] A, int target, int start_index)
    {
        for (int i = start_index, j = A.length - 1; i < j;)
        {
            int sum = A[i] + A[j];
            if (sum == target)
            {
                return true;
            }
            if (sum < target)
            {
                ++i;
            }
            else
            {
                --j;
            }
        }
        return false;
    }

    public static Boolean find_sum_of_three_v3(int arr[], int required_sum)
    {
        Arrays.sort(arr);
        for (int i = 0; i < arr.length-2; ++i)
        {
            int remaining_sum = required_sum - arr[i];
            if (find_sum_of_two(arr, remaining_sum, i + 1))
            {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args)
    {
        int[] array1 = new int[]{5, 7, 1, 2, 8, 4, 3};
        int[] array2 = new int[]{3, 20, 1, 2, 7};

        /*for (int i = 0; i < array2.length; i++)
        {
            boolean output = findSumOfTwo(array1, array2[i]);
            System.out.println("findSumOfTwo(array1, " + array2[i] + ") = " + (output ? "true" : "false"));
        }*/
        int[] arr = {-25, -10, -7, -3, 2, 4, 8, 10};
        System.out.println("-8: " +find_sum_of_three_v3(arr, -8));
        System.out.println("-25: " +find_sum_of_three_v3(arr, -25));
        System.out.println("0: " +find_sum_of_three_v3(arr, 0));
        System.out.println("-42: " +find_sum_of_three_v3(arr, -42));
        System.out.println("22: " +find_sum_of_three_v3(arr, 22));
        System.out.println("-7: " +find_sum_of_three_v3(arr, -7));
        System.out.println("-3: " +find_sum_of_three_v3(arr, -3));
        System.out.println("2: " +find_sum_of_three_v3(arr, 2));
        System.out.println("4: " +find_sum_of_three_v3(arr, 4));
        System.out.println("8: " +find_sum_of_three_v3(arr, 8));
        System.out.println("7: " +find_sum_of_three_v3(arr, 7));
        System.out.println("1: " +find_sum_of_three_v3(arr, 1));
    }
}
