## Solutions to technical interview questions asked during the Interview process

## SumEqualToX_01 

This is from an interview with __ 

Prompt
```
Java program to find all pairs in both arrays
whose sum is equal to given value x
Function to print all pairs in both arrays
whose sum is equal to given value x
```

## SumEqualToX_02 

This is from an interview with Apple. 
The prompt was: 
```
Given two arrays, find all pairs whose sum is x
     Input:
     array1 = [1,2,4,5,7]
     array2 = [5,6,3,4,8]
     x = 9

     Output:
     1 8
     4 5
     5 4
```

